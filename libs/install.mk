SDK_DIR 	:= ../sdk
SDK_LIB_DIR	:= $(SDK_DIR)/lib
SDK_INC_DIR	:= $(SDK_DIR)/include

.PHONY: all

all: libs
	echo install libs done.

libs:
	echo "install libgcc"
	cp libgcc/arch/$(ARCH)/libgcc.a $(SDK_LIB_DIR)/libgcc.a
	echo "----------"
	echo "install zlib"
	cp build/zlib/libzlib.a $(SDK_LIB_DIR)/libzlib.a
	cp zlib/zconf.h $(SDK_INC_DIR)/zconf.h
	cp zlib/zlib.h $(SDK_INC_DIR)/zlib.h
	echo "----------"
	echo "install libpng"
	cp build/libpng/libpng.a $(SDK_LIB_DIR)/libpng.a
	cp libpng/png.h $(SDK_INC_DIR)/png.h
	echo "----------"
	echo "install jpeg"
	cp build/jpeg/libjpeg.a $(SDK_LIB_DIR)/libjpeg.a
	cp jpeg/*.h $(SDK_INC_DIR)/
	echo "----------"
	echo "install freetype"
	cp build/freetype/libfreetype.a $(SDK_LIB_DIR)/libfreetype.a
	cp freetype/include/ft2build.h $(SDK_INC_DIR)/
	cp -r freetype/include/freetype $(SDK_INC_DIR)/
	echo "----------"
	echo "install libwebp"
	cp build/libwebp/libwebp.a $(SDK_LIB_DIR)/libwebp.a
	echo "----------"
	echo "install libtiff"
	cp build/libtiff/libtiff.a $(SDK_LIB_DIR)/libtiff.a
	echo "----------"
	echo "install SDL2" 
	rm -rf $(SDK_INC_DIR)/SDL2
	cp build/SDL2/libSDL2.a $(SDK_LIB_DIR)/libSDL2.a
	cp -r SDL2/include $(SDK_INC_DIR)/SDL2
	echo "----------"
	echo "install SDL_ttf"
	cp build/SDL2_ttf/libSDL2_ttf.a $(SDK_LIB_DIR)/libSDL2_ttf.a
	cp SDL2_ttf/SDL_ttf.h $(SDK_INC_DIR)/SDL2
	echo "----------"
	echo "install SDL_image"
	cp build/SDL2_image/libSDL2_image.a $(SDK_LIB_DIR)/libSDL2_image.a
	cp SDL2_image/SDL_image.h $(SDK_INC_DIR)/SDL2
	echo "----------"
	echo "install libwm"
	-rm -rf $(SDK_INC_DIR)/wm
	cp build/libwm/libwm.a $(SDK_LIB_DIR)/libwm.a
	cp -r libwm/wm $(SDK_INC_DIR)/wm
	echo "----------"
	echo "install gato"
	-rm -rf $(SDK_INC_DIR)/gato
	cp build/gato/libgato.a $(SDK_LIB_DIR)/libgato.a
	cp -r gato/include/gato $(SDK_INC_DIR)/gato
