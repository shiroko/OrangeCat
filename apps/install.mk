ROOTFS_DIR 	:= ../rootfs
APP_DIR		:= $(ROOTFS_DIR)/Applications
SYS_DIR		:= $(ROOTFS_DIR)/System
SYSAPP_DIR	:= $(SYS_DIR)/Applications
SERVICE_DIR	:= $(SYS_DIR)/Service

.PHONY: all

all: apps tests
	echo install apps done.

apps:
	cp build/UserLand/UserLand.xapp $(SERVICE_DIR)
	cp build/WinManager/WinManager.xapp $(SERVICE_DIR)
	cp build/PowerBox/PowerBox.xapp $(SYSAPP_DIR)
	cp build/infones/infones.xapp $(APP_DIR)
	cp build/gnuboy/gnuboy.xapp $(APP_DIR)
	cp build/showimage/showimage.xapp $(APP_DIR)
	cp build/showfont/showfont.xapp $(APP_DIR)
	cp build/key_test/key_test.xapp $(APP_DIR)

tests:
	cp build/LVGL_Demo/LVGL_Demo.xapp $(APP_DIR)
	cp build/WinTest/WinTest.xapp $(APP_DIR)
	cp build/SDL_Demo/SDL_Demo.xapp $(APP_DIR)
	cp build/sdl_audio/sdl_audio.xapp $(APP_DIR)
