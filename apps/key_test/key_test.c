#include <nxos.h>

#define calibrate	0

NX_Error NX_Main(char *cmdline, char *envline)
{
	NX_ThreadSleep(2000);	// 避免该应用的log被shell打印打乱
	NX_InputEvent e;
	NX_U32 i = 0;

	NX_Printf("[%s %d] APP Start!\n", __func__, __LINE__);

	NX_Solt key0 = NX_DeviceOpen("joystick0", 0);
	if (key0 < 0) {
		NX_Printf("open joystick0 device failed!\n");
		return NX_ENOSRCH;
	}

#if calibrate
	NX_U32 info;
	// 校准键盘测试
	if (NX_DeviceControl(key0, NX_INPUT_EVENT_CMD_CALIBRATE, &info) != NX_EOK)
	{
		NX_Printf("get fb info failed!\n");
		NX_SoltClose(key0);
		return NX_EPERM;
	}
	NX_Printf("info:%d\n", info);
#endif

	NX_U32 run = 1;
	while (run) {
		if (NX_DeviceRead(key0, &e, 0, sizeof(e)) > 0) {
			if (e.type == NX_EV_KEY) {
				NX_Printf("%s: %x/%d\n", e.value ? "keydown" : "keyup", e.code, e.code);
			}
		}
		if (i++ > 1000)
			run = 0;
		NX_ThreadSleep(10);
	}
	NX_SoltClose(key0);

	NX_Printf("[%s %d] APP Run finish!\n", __func__, __LINE__);
	return 0;
}