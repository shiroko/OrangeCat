MAKE			:= make -s -C  # make in sub dir
ARCH			?= x86
PLATFORM		?= i386
CROSS_COMPILE	?=
GIT				:= git
TRUNC			:= truncate
MCOPY			:= mtools -c mcopy
MKFS			:= mkfs.fat
CPIO			:= cpio -o -H newc --quiet
CP				:= cp

KERNEL			:= kernel
LIBNXOS			:= lib-nxos
LIBXLIBC		:= lib-xlibc
LIBS_DIR		:= libs
APPS_DIR		:= apps

SDK_DIR		:= ./sdk
SDK_INC_DIR	:= $(SDK_DIR)/include
SDK_LIB_DIR	:= $(SDK_DIR)/lib

#
# Rootfs
#
ROOTFS_DIR	:= rootfs
ROOTFS_CPIO	:= rootfs.cpio
ROMDISK_S	:= romdisk.S

ROOTFS_SIZE	:= 64M
ROOTFS_IMG	:= rootfs.img

cur_mkfile := $(abspath $(lastword $(MAKEFILE_LIST)))
CWD	:= $(dir $(cur_mkfile))
$(info CWD=$(CWD))

ifeq ($(OS), Windows_NT)
	ifeq ($(ARCH), x86)
		CROSS_COMPILE:=x86_64-elf-
	else ifeq ($(ARCH), riscv64)
		CROSS_COMPILE:=riscv-none-embed-
	else
		$(error unsupportted arch $(ARCH))
	endif
else
	ifeq ($(ARCH), x86)
		CROSS_COMPILE:=
	else ifeq ($(ARCH), riscv64)
		CROSS_COMPILE:=riscv64-unknown-elf-
	else
		$(error unsupportted arch $(ARCH))
	endif
endif

export ARCH PLATFORM CROSS_COMPILE

.PHONY: build clean libs apps run prepare

build: all_apps
	echo build kernel
	export NXOS_SRC_DIR=$(CWD)/$(KERNEL)/src
	cp $(CWD)/$(KERNEL)/configs/platform-$(PLATFORM).mk $(CWD)/$(KERNEL)/platform.mk
	$(MAKE) $(KERNEL) defconfig
	$(MAKE) $(KERNEL)
	echo build kernel $(ARCH)-$(PLATFORM) done

baselibs:
	$(MAKE) $(LIBNXOS) 
	$(MAKE) $(LIBNXOS) install
	$(MAKE) $(LIBXLIBC)
	$(MAKE) $(LIBXLIBC) install	
	echo build baselibs done.

all_libs: baselibs
	@$(MAKE) $(LIBS_DIR) O=build
	@$(MAKE) $(LIBS_DIR) -f install.mk
	echo build libs done.

libs:
	@$(MAKE) $(LIBS_DIR) O=build
	@$(MAKE) $(LIBS_DIR) -f install.mk
	echo build libs done.

all_apps: all_libs
	@$(MAKE) $(APPS_DIR) O=build
	@$(MAKE) $(APPS_DIR) -f install.mk
	echo build apps done.

apps:
	@$(MAKE) $(APPS_DIR) O=build
	@$(MAKE) $(APPS_DIR) -f install.mk
	echo build apps done.

rootfs: apps
	@cd $(ROOTFS_DIR) && \
		find . -not -name . | $(CPIO) > ../$(ROOTFS_CPIO) && \
		cd ..
	@cp $(ROOTFS_CPIO) $(KERNEL)
	@cp $(ROMDISK_S) $(KERNEL)/src/drivers/block/
	@echo make rootfs cpio done.
	-rm $(ROOTFS_IMG)
	$(TRUNC) --size $(ROOTFS_SIZE) $(ROOTFS_IMG)
	$(MKFS) -F 32 $(ROOTFS_IMG)
	-$(MCOPY) -i $(ROOTFS_IMG) -/ $(ROOTFS_DIR)/* ::./
	@$(CP) $(ROOTFS_IMG) $(KERNEL)
	@echo make rootfs image done.

run: rootfs
	$(MAKE) $(KERNEL) run GUI=y HD=y SND=y

clean:
	-$(MAKE) $(LIBNXOS) clean
	-$(MAKE) $(LIBXLIBC) clean
	-$(MAKE) $(LIBS_DIR) clean
	-$(MAKE) $(APPS_DIR) clean
	-$(MAKE) $(KERNEL) clean
	-rm $(ROOTFS_CPIO)
	-rm $(ROOTFS_IMG)
	-rm $(KERNEL)/$(ROOTFS_CPIO)
	-rm $(KERNEL)/$(ROOTFS_IMG)
	-rm $(KERNEL)/src/drivers/block/$(ROMDISK_S)
	echo clean done.

prepare:
	$(GIT) submodule init
	$(GIT) submodule update
